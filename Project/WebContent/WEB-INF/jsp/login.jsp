<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html">
<html>
<head>
<meta charset="UTF-8">
 <title>ユーザリスト</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <br>
    <br>
    <div class="text-center"><h1>ログイン画面</h1></div>
	<c:if test="${errMsg != null}" >
	<div class="col text-danger">
	${errMsg}</div></c:if>
    <br>
    <br>
    <br>
    <br>
    <br>

<%--LoginServlet.javaクラスへリクエストパラメータをPOSTリクエストする処理 --%>
 <form action="/UserManagement/LoginServlet" method="post">

    <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-3">ログインID</div>
    <div class="col-sm-5"><input type="text" class="form-control" name="loginId"></div>
    <div class="col-sm-2"></div>
  </div>
    <br>
 	<div class="row">
 	<div class="col-sm-2"></div>
    <div class="col-sm-3">パスワード</div>
    <div class="col-sm-5"><input type="password" class="form-control" name="password"></div>
    <div class="col-sm-2"></div>
  </div>
    <br>
    <br>
    <br>
  <p class="text-center"><input type="submit" class="col-2" value="ログイン"></p>
</form>
</div>
</body>
</html>
