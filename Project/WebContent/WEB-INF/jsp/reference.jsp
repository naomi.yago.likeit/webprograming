<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <title>ユーザリスト</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<div class="row text-right">
	<div class="col p-2 mb-2 bg-secondary text-white">ユーザ名 ${sessionScope.userInfo.name} さん
    <a href="LogoutServlet" class="text-danger">ログアウト</a></div>
    </div>
	<br>
    <div class="text-center"><h1>ユーザ情報詳細参照</h1></div>
    <br>
    <br>
<form>
	<div class="row">
    <div class="col">ログインID</div>
    <div class="col"><input type="text" readonly class="form-control-plaintext" id="loginId">${user.loginId}
    </div></div>
     <p></p>
    <div class="row">
    <div class="col">ユーザ名</div>
    <div class="col"><input type="text" readonly class="form-control-plaintext" id="userName">${user.name}
    </div></div>
    <p></p>
    <div class="row">
    <div class="col">生年月日</div>
    <div class="col"><input type="text" readonly class="form-control-plaintext" id="birth">${user.birthDate}
    </div></div>
    <p></p>
    <div class="row">
    <div class="col">登録日時</div>
    <div class="col"><input type="text" readonly class="form-control-plaintext" id="addDate">${user.createDate}
    </div></div>
    <p></p>
    <div class="row">
    <div class="col">更新日時</div>
    <div class="col"><input type="text" readonly class="form-control-plaintext" id="upDate">${user.updateDate}
    </div></div>
  	<br>
    <br>
</form>
	<p class="text-left"><a href="UserListServlet">戻る</a></p>
</div>
</body>
</html>
