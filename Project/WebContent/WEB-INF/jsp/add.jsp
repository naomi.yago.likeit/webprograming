<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <title>ユーザリスト</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<div class="container">
  	<div class="row text-right">
 	<div class="col p-2 mb-2 bg-secondary text-white">ユーザ名 ${sessionScope.userInfo.name} さん
 	<a href="LogoutServlet" class="text-danger">ログアウト</a></div>
 	</div>
	<br>
    <div class="text-center"><h1>ユーザ新規登録</h1></div>
    <c:if test="${errMsg2 != null}" >
    <div class="col text-danger">
	${errMsg2}</div></c:if>
    <br>
    <br>
<form action="AddServlet" method="post">
	<div class="row">
    <div class="col">ログインID</div>
    <div class="col">
      <input type="text" class="form-control" name="formLoginId" value="${formLoginId}">
    </div></div>
    <p></p>
	<div class="row">
    <div class="col">パスワード</div>
    <div class="col">
     <input type="password" class="form-control" name="password1">
     </div></div>
	<p></p>
	<div class="row">
    <div class="col">パスワード(確認)</div>
    <div class="col">
     <input type="password" class="form-control" name="password2">
     </div></div>
     <p></p>
     <div class="row">
    <div class="col">ユーザ名</div>
    <div class="col">
      <input type="text" class="form-control" name="userName" value="${name}">
    </div></div>
    <p></p>
     <div class="row">
    <div class="col">生年月日</div>
    <div class="col">
      <input type="date" class="form-control" name="birth" value="${birth}">
    </div></div>
    <p></p>
  	<br>
    <br>
	<p class="text-center"><input type="submit" class="col-2" value="登録"></p>
</form>
	<p class="text-left"><a href="UserListServlet">戻る</a></p>
</div>
</body>


</html>
