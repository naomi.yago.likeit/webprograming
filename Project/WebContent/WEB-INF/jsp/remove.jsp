<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <title>ユーザリスト</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<div class="row text-right">
	<div class="col p-2 mb-2 bg-secondary text-white">ユーザ名 ${sessionScope.userInfo.name} さん
    <a href="LogoutServlet" class="text-danger">ログアウト</a></div>
    </div>
	<br>
    <div class="text-center"><h1>ユーザ削除確認</h1></div>
    <br>
	<div class="row">
    <div class="col">ログインID： ${user.loginId}</div>
    </div>
    を本当に削除してもよろしいでしょうか。
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="row">
	<div class="col text-center">
	<a href="UserListServlet"><input type="submit" class="col-4" value="キャンセル"></a></div>
	<div class="col text-center">
	<form action="RemoveServlet" method="post">
	<input type="hidden" name="id" value="${user.id}">
	<input type="submit" class="col-4" value="OK"></form></div>
	</div>

</div>
</body>
</html>