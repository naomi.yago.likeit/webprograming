<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <title>ユーザリスト</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<div class="container">
	<div class="row text-right">
	<div class="col p-2 mb-2 bg-secondary text-white">ユーザ名 ${sessionScope.userInfo.name} さん
    <a href="LogoutServlet" class="text-danger">ログアウト</a></div>
    </div>
  	<br>
    <div class="text-center"><h1>ユーザ一覧</h1></div>
    <br>

    <div class="col text-right">
    <a href="/UserManagement/AddServlet?">新規登録</a></div>

<form action="/UserManagement/UserListServlet" method="post">
	<div class="row">
    <div class="col-sm-3">ログインID</div>
    <div class="col-sm-6"><input type="text" class="form-control" name="loginId"></div>
    <div class="col-sm-3"></div></div>
    <br>

    <div class="row">
    <div class="col-sm-3">ユーザ名</div>
    <div class="col-sm-6"><input type="text" class="form-control" name="userName"></div>
    <div class="col-sm-3"></div></div>
    <br>

    <div class="row">
    <div class="col-sm-3">生年月日</div>
    <div class="col-sm-3"> <input type="date" class="form-control" name="calendar1" max="9999-12-31"></div>
    <div class="col-sm-1">～</div>
    <div class="col-sm-2"> <input type="date" class="form-control" name="calendar2" max="9999-12-31"></div>
    <div class="col-sm-3"></div></div>
	<br>

	<p class="text-right"><input type="submit" class="col-3" value="検索"></p>
</form>

	<hr>

<table class="table table-bordered table-sm">
<thead>
    <tr>
      <th scope="col">ログインID</th>
      <th scope="col">ユーザ名</th>
      <th scope="col">生年月日</th>
      <th scope="col"></th>
    </tr>
</thead>
   <tbody>
   <c:if test="${userInfo.loginId=='admin'}">
   <c:forEach var="user" items="${userList}" >
    <tr>
       <td>${user.loginId}</td>
       <td>${user.name}</td>
       <td>${user.birthDate}</td>
      <td>
      <a href="ReferenceServlet?id=${user.id}"><button type="button" class="col-3 btn btn-primary btn-sm">詳細</button></a>
      <a href="UpdateServlet?id=${user.id}"><button type="button" class="col-3 btn btn-success btn-sm">更新</button></a>
      <a href="RemoveServlet?id=${user.id}"><button type="button" class="col-3 btn btn-danger btn-sm">削除</button></a>
      </td>
    </tr>
    </c:forEach>
    </c:if>

     <c:if test="${userInfo.loginId!='admin'}">
   <c:forEach var="user" items="${userList}" >
    <tr>
       <td>${user.loginId}</td>
       <td>${user.name}</td>
       <td>${user.birthDate}</td>
       <td>
      <a href="ReferenceServlet?id=${user.id}"><button type="button" class="col-3 btn btn-primary btn-sm">詳細</button></a>
	  <c:if test="${userInfo.loginId==user.loginId}">
      <a href="UpdateServlet?id=${user.id}"><button type="button" class="col-3 btn btn-success btn-sm">更新</button></a></c:if>
      </td>
    </tr>
    </c:forEach>
    </c:if>
   </tbody>
</table>
  <br>
  <br>

</div>
</body>
</html>