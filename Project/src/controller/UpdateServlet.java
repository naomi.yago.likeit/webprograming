package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		//GET
		String id = request.getParameter("id");



		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User user=userDao. ShowUserDetail(id);

		request.setAttribute("user",user);


		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//パラメータの取得
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String pass = request.getParameter("password1");
		String pass2 = request.getParameter("password2");
		String name = request.getParameter("userName");
		String birth = request.getParameter("birth");



		/** 更新失敗時 **/
		if (!pass.equals(pass2)||name.equals("")||birth.equals("")) {

			Date dateBirth = null;
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			 	try {
			 		 dateBirth = sdf.parse(birth);
			 		request.setAttribute("birthDay",dateBirth);

			 	} catch (ParseException e) {
		            e.printStackTrace();
			 	}

			 	int idind = Integer.parseInt(id);

			User user=new User(idind, loginId, name, dateBirth, null, null, null);

			request.setAttribute("user",user);

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg3","入力された内容は正しくありません");

			// 更新jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;

		} else if(pass.equals("")&&pass2.equals("")) {

			UserDao userDao = new UserDao();
			userDao.UpdateOtherThanPass(id,name,birth);

			response.sendRedirect("UserListServlet");

			/** 成功時 **/
		}else{
			UserDao userDao = new UserDao();
			userDao.UserUpdate(id, pass,name,birth);

		// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
		}

	}

}
