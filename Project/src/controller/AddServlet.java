package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

@WebServlet("/AddServlet")
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("LoginServlet");
			return;
		}


		//フォワード
				RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/add.jsp");
				dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//登録 のリクエストパラメータの取得
		request.setCharacterEncoding("UTF-8");

		String formLoginId = request.getParameter("formLoginId");
		String pass = request.getParameter("password1");
		String pass2 = request.getParameter("password2");
		String name = request.getParameter("userName");
		String birth = request.getParameter("birth");

		//判定用のメソッド
		UserDao userDao=new UserDao();
		boolean isNoSelectUserData=userDao.IdJudge(formLoginId);

		 //登録失敗時
		 if (!pass.equals(pass2)||formLoginId.equals("")||pass.equals("")||pass2.equals("")||
				 name.equals("")||birth.equals("")||!isNoSelectUserData) {
			    request.setAttribute("formLoginId",formLoginId);
			    request.setAttribute("name",name);
			    request.setAttribute("birth",birth);
			 // リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg2","入力された内容は正しくありません");

		 //フォワード
		 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/add.jsp");
			dispatcher.forward(request, response);
			return;
		 }

		 //成功時
		 userDao.addUser(formLoginId, pass, name, birth);
		userDao.stragePassword(pass);
		 //確認用
		 System.out.println();
		// System.out.println(user);
		// ユーザ一覧のサーブレットにリダイレクト
		 response.sendRedirect("UserListServlet");


	}
}